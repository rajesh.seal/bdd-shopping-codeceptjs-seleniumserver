Feature: Shopping Order Checkout Process
  In order to buy products
  As a customer
  I want to be able to add product to cart and check out

  Background:
    Given I am already logged in shopping portal

    
  Scenario: Add 2 items to cart and checkout
    Given I see my account page
    When I add 2 items to my cart
    And I perform checkout process
    Then I should be able to sucessfully place order