# BDD-Shopping-CodeceptJS-SeleniumServer
---
About:
The project is to demonstrate shopping feature wherein a user buys 2 items and checks out.

---
##To run test on local machine
Clone repo on your system. Please make sure you have `NodeJS` installed on your system.

1. Run `npm install` to install dependencies on system.
2. Run `npm run selenium-server:install` to install server binaries.
3. Run `npm run selenium-server:start` to start selenium server.
4. Run `npm test` on seperate terminal to run tests.
 
---
##Note about config:
1. Tests will run in headless mode by default. Please comment `desiredCapabilities` in `codecept.conf.js` file to run in browser mode.
2. Reports: By default screenshot is being captured on `failure`. To enable full report with all screen captures at every step and get a html output please change value to `enabled: true` in `stepByStepReport` plugin present at `codecept.conf.js` file.

---
##Asssumtions:
1. AUT is `up and running` with `no change` to workflow.

---
##Enhancements:
1. Addition of more business assertions.
2. Addition of data table to simulate the test for multiple users.
3. Configuring the project for Gitlab CI.
4. Need to move the validations strings to page files.
---