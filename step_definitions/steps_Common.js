
const { I , loginPage} = inject();
// common function for login
// need to add login creds to data table
Given('I am already logged in shopping portal', () => {
    // From "features\shopping.feature" {"line":7,"column":5}
    I.amOnPage(loginPage.siteURL)
    I.click(loginPage.signIn);
    I.fillField(loginPage.userName, "DemoBuyer@gmail.com");
    I.fillField(loginPage.passWord, "DemoBuyer!");
    I.click({css: '#SubmitLogin'});
  });
  