const { I , checkoutPage , productPage } = inject();
const assert = require('assert');

Given('I see my account page', () => {
  I.see("Welcome to your account. Here you can manage all of your personal information and orders.");
});

When('I add {int} items to my cart', (num) => {
  // From "features\shopping.feature" {"line":12,"column":5}
     for (let i = 1; i <= num; i++) {
        I.click(productPage.productCategory);   //click on product category
        I.scrollIntoView(productPage.scrollToProduct);  //get product list in to view 
        I.scrollTo(productPage.productListView)   //focus on product list
        I.click('li.ajax_block_product:nth-child('+i+')')   //iterate and click on product from list
        I.click(productPage.addToCart)  //Click add to cart
        I.click(productPage.continueShoppingBtn)  //click continue shopping
    } 
    I.see(num,productPage.itemInCart); //Check the count of products in cart is 2
});

When('I perform checkout process', async () => {
  I.click(checkoutPage.clkShoppingCart);  //click on shoppng cart
  I.see("SHOPPING-CART SUMMARY"); //Check text shopping cart summary
  I.scrollTo(checkoutPage.focusCartTable) //validate 2 items exist
  let numOfCartItems =  await I.grabNumberOfVisibleElements(checkoutPage.countCartItems);
  assert.equal(numOfCartItems, 2);
  I.scrollTo(checkoutPage.navCheckout);   //proceed with checkout
  I.click(checkoutPage.navCheckout);
  I.fillField(checkoutPage.txtDeliveryNote, 'Please call before delivery'); //enter delivery notes and proceed
  I.click(checkoutPage.clkCheckOutAddressTab);  //Click checkout on address tab
  I.click(checkoutPage.chkTermsCondsShippingTab); //click on checkbox Terms of Service and proceed 
  I.click(checkoutPage.clkCheckOutShippingTab);   // click checkout button on Shipping tab
  I.scrollTo(checkoutPage.focusPayOptions);
  I.click(checkoutPage.selectChequeOpt);    //select pay by cheque
  I.see('You have chosen to pay by check');
  I.click(checkoutPage.btnConfirmOrder);   //click on confirm order button
});

Then('I should be able to sucessfully place order', async () => {
  I.see('Your order on My Store is complete.');
  I.scrollTo(checkoutPage.focusOrderConfirmation); //scroll to the order confirmation box
  I.see('YOUR CHECK MUST INCLUDE:'); //validate text in order about check details
  I.see('Do not forget to include your order reference');
  let ecartVal = await I.grabTextFrom(checkoutPage.getTotalCartPrice);
  I.click(checkoutPage.lnkOrderHistory);  //navigate to order history
  let priceLastOrder =  await I.grabTextFrom(checkoutPage.getLastOrderPriceHistory); //get and validate the price value of latest order
  assert.equal(priceLastOrder, ecartVal);
});