const { I } = inject();
//page object locators for product  page
module.exports = 
{
        productCategory: {css: '.sf-menu > li:nth-child(1) > a:nth-child(1)'},
        scrollToProduct: {css: '.product_list'},
        productListView: {css: 'li.ajax_block_product:nth-child(1)'},
        secondItem: {css: 'li.ajax_block_product:nth-child'},
        firstItem: {css: 'li.ajax_block_product:nth-child(1)'},
        addToCart: {css: 'button.exclusive > span:nth-child(1)'},
        continueShoppingBtn: {css: '.continue'},
        itemInCart: {css: 'span.ajax_cart_quantity:nth-child(2)'}
                     
};        