const { I } = inject();
//page object locators for checkout process
module.exports = 
{
        clkShoppingCart: {css: '.shopping_cart > a:nth-child(1)'},
        focusCartTable: {css: '#cart_title'},
        countCartItems: {css: '.cart_item'},
        navCheckout: {css: '.standard-checkout'},
        txtDeliveryNote: {css: 'textarea.form-control'},
        clkCheckOutAddressTab: {css: 'button[name="processAddress"]'},
        chkTermsCondsShippingTab: {css: '.checkbox > label:nth-child(2)'},
        clkCheckOutShippingTab: {css: 'button[name="processCarrier"]'},
        focusPayOptions: {css: '#HOOK_PAYMENT'},
        selectChequeOpt: {css: '.cheque'},
        btnConfirmOrder: {css: 'button.button-medium'},
        focusOrderConfirmation: {css: '.box'},
        getTotalCartPrice: {css: 'span.price:nth-child(2)'},
        lnkOrderHistory: {css: '.button-exclusive'},
        getLastOrderPriceHistory: {css: 'tr.first_item > td:nth-child(3)'}

};