const { I } = inject();
//page object locators for sign-in page
module.exports = 
{
        userName: {css: '#email'},
        passWord: {css: '#passwd'},
        signIn: {css: '.login'},
        siteURL: 'http://automationpractice.com/index.php'                
};