exports.config = {
  output: './output',
  helpers: {
    WebDriver: {
      url: 'localhost',
      browser: 'firefox',
          desiredCapabilities: {
            "moz:firefoxOptions": {
              "args": [
                "--headless"        ]
            }
          }
    }
  },
  include: {
    I: './steps_file.js',
    checkoutPage: './pages/checkoutPage.js', 
    loginPage: './pages/loginPage.js',
    productPage: './pages/productPage.js'
  },
  mocha: {},
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: ['./step_definitions/steps_Checkout.js','./step_definitions/steps_Common.js']
  },
  plugins: {
    retryFailedStep: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    },
    desiredCapabilities: {
      // close all unexpected popups (rase added)
      unexpectedAlertBehaviour: 'dismiss',
    },
    stepByStepReport: {  //(rase added)
      "enabled": false,
      "deleteSuccessful": false
    }
  },
  tests: './test/*_test.js',
  name: 'bdd-shopping-codeceptjs-seleniumserver'
}